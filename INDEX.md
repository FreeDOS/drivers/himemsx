# HIMEMSX

HimemSX is a fork of HimemX. Its main feature is that it's able to manage more than 4 GB of memory.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## HIMEMSX.LSM

<table>
<tr><td>title</td><td>HIMEMSX</td></tr>
<tr><td>version</td><td>3.54</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-04-12</td></tr>
<tr><td>description</td><td>Fork of HimemX supporting over 4GB of memory</td></tr>
<tr><td>summary</td><td>HimemSX is a fork of HimemX. Its main feature is that it's able to manage more than 4 GB of memory.</td></tr>
<tr><td>keywords</td><td>extended memory, himem, himemsx, memory manager, super extended, XMS</td></tr>
<tr><td>author</td><td>Andreas "Japheth" Grech</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/HimemSX</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[General Public License, Artistic license and Public Domain](LICENSE)</td></tr>
</table>
